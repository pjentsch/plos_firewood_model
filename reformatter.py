import re


dir = "data\\"


pts = open(dir + "EAB_points.csv")

rts = open(dir + "25Mar13_CanUS_RTS.txt")



pts_directory = {}

for line in pts.readlines()[1:]:
    line_format = re.split(',', line)
    pts_directory[line_format[2]] = (line_format[8],line_format[9])

output= open(dir + "EAB_trips.txt",'w')
output.write("ID;NTRIPS;wkt\n")

array_output = open(dir + "EAB_trips_array.txt",'w')

route_directory = {}
for line in rts.readlines()[1:]:
    line_format = re.split('\t', line[:-1])
    route_directory[(line_format[3],line_format[4])] = (line_format[2])

for i,key in enumerate(route_directory.keys()):
    value = route_directory[key]
    destination = key[1]
    start = key[0]
    trips = value[0]
    if start in pts_directory and destination in pts_directory:
        writestring = str(i)+";"+trips+";LINESTRING("+pts_directory[start][0]+" "+pts_directory[start][1]+","+pts_directory[destination][0]+" "+pts_directory[destination][1]+")"
        output.write(writestring+"\n")



points = list(pts_directory.keys())
array_output.write(str(len(points))+"\n")
pointlist = open( dir + "localpointlist.txt",'w')
totalstring = ""
for i,key1 in enumerate(points):
    outstring = ""
    for j,key2 in enumerate(points):
        if key1 != key2 and (key1,key2) in route_directory:
            outstring += route_directory[(key1,key2)] + ","
        else:
            outstring+= "0,"
    outstring = outstring[:-2]+ "|"
    totalstring = totalstring + outstring
    pointlist.write(key1 + "\n")

pointlist.close()

array_output.write(totalstring[:-1])


array_output.close()
pts.close()
rts.close()
output.close()