import matplotlib.pyplot as pp
import os
import math
import numpy as np
from collections import defaultdict
import seaborn as sns
import matplotlib.cm as cm
import string
from matplotlib.colors import Normalize
from mpl_toolkits.mplot3d import axes3d, Axes3D
from numpy.core._multiarray_umath import ndarray
imgformat = ".png"

class MidpointNormalize(Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))
def parse_folder_julia(filename,num_timeseries):
    nested_dict = lambda: defaultdict(nested_dict)
    data_dict = nested_dict()
    file = np.load(filename,allow_pickle=True)
    times,output_array,ranges = file
    ranges = list(map(list,ranges))
    return times,ranges,output_array

def setlabel(ax, label, loc=2, borderpad=0.6, **kwargs):
    legend = ax.get_legend()
    if legend:
        ax.add_artist(legend)
    line, = ax.plot(np.NaN,np.NaN,color='none',label=label)
    label_legend = ax.legend(handles=[line],loc=loc,handlelength=0,handleheight=0,handletextpad=0,borderaxespad=0,borderpad=borderpad,frameon=False,**kwargs)
    label_legend.remove()
    ax.add_artist(label_legend)
    line.remove()

sns.set(style="whitegrid", font_scale=1.8)

dir = "plots/"


def get_data_from_folder(timeseries_folder,num_timeseries):
    times, param_list, datamap = parse_folder_julia(timeseries_folder, num_timeseries)
    return times,param_list,datamap


def plot_marginal_gain(times,xlist,ylist,marginal_list,data_map,title,xlabel,ylabel,filename,dt,timelist):
    fig_ts, ax_ts = pp.subplots(1, len(times), figsize=(30, 8))
    grids = [np.zeros((len(xlist), len(ylist))) for i in ax_ts]
    for num, (ax, grid, time) in enumerate(zip(ax_ts, grids, times)):
        for i, x in enumerate(xlist):
            for j, y in enumerate(ylist):
                slicelist = []
                for k,p in enumerate(marginal_list):
                    ts_list = data_map[i,k,j]
                    slicelist.append(ts_list[4][round(time/dt)])
                grid[i, j] = np.polyfit(marginal_list, slicelist, 1)[0]
                if grid[i,j]>0.0:
                    print(str(num) + "," + str(i) + ", "  + str(j) + ", " + str(grid[i,j]))
                

        norm = MidpointNormalize(midpoint=0,vmax= np.max(grid))
        im = ax.imshow(list(reversed(np.transpose(grid))), interpolation='nearest',
                       extent=(min(xlist), max(xlist), min(ylist), max(ylist)),
                       cmap=cm.get_cmap("Blues_r"), aspect='auto')

        cb = pp.colorbar(im, ax=ax)
        setlabel(ax, string.ascii_lowercase[num] + ")", borderpad=-1.9)
        ax.set_title("After " + str(time) + " years")
        ax.set_xlabel(xlabel)
        cb.set_label("Total infested at $t = " + str(time) + "$ (T(" + str(time) + ")) per U")
    fig_ts.suptitle(title)
    ax_ts[0].set_ylabel(ylabel)
    pp.savefig(filename +imgformat)

def plot_3d_inset(timelist,xlist,ylist,marginal_list,data_map,title,xlabel,filename1,filename2,dt,point_3d):
    fig_ts, ax_ts = pp.subplots(1, 1, figsize=(17,8))
    norm = MidpointNormalize(midpoint=0)
    fig_3d = pp.figure(figsize=(12, 7))
    ax_3d = fig_3d.add_subplot(111, projection='3d')
    blues_r_cmap = cm.get_cmap("Blues_r")

    greys = cm.get_cmap("Greens")
    grid = np.zeros((len(xlist), len(ylist)))
    for i, cp_1 in enumerate(xlist):
        for j, t in enumerate(ylist):
            slicelist = []
            for counter, p in enumerate(marginal_list):
                ts_list = data_map[counter,i]
                if cp_1 == point_3d:
                    ax_3d.plot(timelist, ts_list[4], zs=p, zdir='x', color=greys(int((p / max(marginal_list) * 100) + 155)))

                slicelist.append(ts_list[4][round(t / dt)])
            m, b = np.polyfit(marginal_list, slicelist, 1)
            grid[i, j] = m
            if cp_1 == point_3d:
                print(1 - math.log(m*-1)/5.5)
                ax_3d.plot(marginal_list, m * np.array(marginal_list) + b, zs=t, zdir='y', color = blues_r_cmap(1 - m/-240))

    im = ax_ts.imshow(list(reversed(np.transpose(grid))), interpolation='nearest',
                      extent=(min(xlist), max(xlist), min(ylist), max(ylist)),
                       cmap=cm.get_cmap("Blues_r"), aspect='auto')

    cb = pp.colorbar(im, ax=ax_ts)

    ax_ts.set_ylabel("$time (years) $")

    cb.set_label("Total infested at $t$ (T(t)) per U")
    ax_ts.set_xlabel(xlabel)
    ax_ts.set_title(title)

    ax_3d.view_init(elev=20, azim=-35)
    ax_3d.xaxis.set_tick_params(pad=10)
    ax_3d.yaxis.set_tick_params(pad=10)
    ax_3d.zaxis.set_tick_params(pad=10)
    ax_3d.set_xticks(marginal_list)
    ax_3d.set_xlabel("$U$", labelpad=30)
    ax_3d.set_zlabel("T(t)", labelpad=20)
    ax_3d.set_ylabel("time (years)", labelpad=30)
    ax_3d.set_title("Linear approximations of $U$ effectiveness, $f = 0.11$")

    pp.tight_layout()
    fig_ts.savefig(filename1)

    fig_3d.savefig(filename2)

def get_timeseries_plot(axes,times,parameter_list,variable_name, data_map,num,label,legend_flag = False):
    for i, p in enumerate(parameter_list):
        Reds = pp.get_cmap('Reds')
        ts = data_map[i]
        print("length:" + str(len(ts)))#
        axes.plot(times[:], ts[:], label=label+ " = " + '%.2f'%(p),
                      color=Reds(int(((p - min(parameter_list)) / max(parameter_list) )* 165) + 90))

        setlabel(axes, string.ascii_lowercase[num] + ")", borderpad=-1.2)
        axes.set_ylabel(variable_name)
        if legend_flag and i == len(parameter_list)-1:
            legend_x = 1
            legend_y = 0.5
            axes.legend(loc='center left', bbox_to_anchor=(legend_x, legend_y))

def parameter_plane(times,xlist,ylist, data_map, xlabel,ylabel,title,filename,dt):
    norm = MidpointNormalize(midpoint=0)
    fig_ts, ax_ts = pp.subplots(1, len(times), figsize=(24, 8))
    grids = [np.zeros((len(xlist), len(ylist))) for i in times]
    for i, x in enumerate(xlist):
        for j, y in enumerate(ylist):
            ts_list = data_map[i,j]
            for grid,time in zip(grids, times):
                grid[i, j] = data_map[i,j,round(time / dt)]
    for i, (grid, ax, time) in enumerate(zip(grids, ax_ts, times)):
        ax.set_xlabel(xlabel)
        im = ax.imshow(list(reversed(np.transpose(grid))), interpolation='nearest',
                       extent=(min(xlist), max(xlist), min(ylist), max(ylist)),
                       cmap=cm.get_cmap("Oranges"), aspect='auto')  # ,vmin = 1,vmax = 10)
        cb = pp.colorbar(im, ax=ax)
        cb.set_label("T(" + str(time) + ")")
        ax.set_title("After " + str(time) + " years")
        setlabel(ax, string.ascii_lowercase[i] + ")", borderpad=-1.3)
    ax_ts[0].set_ylabel(ylabel)
    fig_ts.suptitle(title)
    pp.savefig(filename)

folderlist = ["f_v_ct_v_ce_d_0_5","A_v_ct_v_d","epsilon_v_ct_v_r","node_control_betweenness"]
param_names = [["C_e","U","f"],["d","U","A"],["r","U","\\gamma"],["t_0","\Delta t","|V|"]]
julia_folder_list = ["C:\\Users\\peter\\Documents\\firewood_model_julia\\control_delay_dt_graph_adj_quarantine.npy"]

description_list = [["Fraction of intercepted firewood","Social incentive to buy locally","Influence of local infestation on strategy"],
                    ["inter-patch infestation rate","Social incentive to buy locally","intra-patch infestation rate"],
                    ["Forest growth rate","Social incentive to buy locally","Infested wood decay rate"],
                    ["Beginning of quarantine","Length of quarantine","number of nodes in "]]

variable_list = ["S(t)", "I(t)", "B(t)","L(t)", "T(t)","# infected patches"]

num_timeseries = 5

plot_folder = ""

def f_v_ct_v_ce_plots():
    #times,param_lists,f_v_ct_v_ce_data = get_data_from_folder("f_v_ct_v_ce_d_0_5", num_timeseries)
    times,param_lists,f_v_ct_v_ce_data = get_data_from_folder("simulation_output/f_U_C_e.npy", num_timeseries)
    dt = 0.01
    print(param_lists)
    variables_to_plot = [0,1, 2, 3]
    fig, axes_list = pp.subplots(len(variables_to_plot), 2, figsize=(18,4 * len(variables_to_plot)))
    for ax,var in zip(axes_list[:,0],variables_to_plot):
        get_timeseries_plot(ax, times, param_lists[2][::2], variable_list[var], f_v_ct_v_ce_data[-3,0,:,var],var,"$C_e$",legend_flag=True)
    for ax,var in zip(axes_list[:,1],variables_to_plot):
        get_timeseries_plot(ax, times, param_lists[1][1::2], variable_list[var], f_v_ct_v_ce_data[-3,:,0,var],var + 4,"$U$",legend_flag=True)

    pp.tight_layout()
    pp.savefig(dir + plot_folder + "timeseries" + imgformat)
    parameter_plane([5,10,20],param_lists[2], param_lists[1],np.transpose(f_v_ct_v_ce_data[-3,:,:,4,:],(1,0,2)),
                    "$C_e$, fraction of intercepted firewood",
                    "$U$, social incentive to buy locally",
                    "Total infested, T(t), with respect to social incentives ($U$), and direct interception ($C_e$), f = 0.01",
                    dir + plot_folder + "ct_v_ce_plane" + imgformat,
                    dt)

    point_3d = 0.11666666666666667

    plot_3d_inset(times,param_lists[0],list(range(5,40,2)),param_lists[1],np.transpose(f_v_ct_v_ce_data[:,:,0,:,:],(1,0,2,3)),
                  "Marginal returns on social incentives ($U$) with respect to $f$ and time",
                  "$f$, influence of local infestation on strategy",
                  dir + plot_folder + "f_v_time_marginal_gain" + imgformat,
                  dir + plot_folder + "3d_inset" + imgformat,
                  dt,
                  point_3d)
def A_v_ct_v_d_plots():
    dt = 0.01
    times, param_lists, A_v_ct_v_d_data = get_data_from_folder(
        "simulation_output/A_U_d.npy", num_timeseries)

    fig, ax = pp.subplots(1, 1, figsize=(11,4))
    get_timeseries_plot(ax, times, param_lists[1][::],4, A_v_ct_v_d_data[6,:,3,4],4,"$U$",legend_flag=True)
    pp.tight_layout()
    pp.savefig(dir + plot_folder + "timeseries_cumulative_zoomed" + imgformat)

    variables_to_plot = [0,1, 2, 3, 4, 5]
    fig, axes_list = pp.subplots(len(variables_to_plot), 1, figsize=(18,4 * len(variables_to_plot)))
    print(str(param_lists[0][3]) + ", " + str(param_lists[2][1]))
    for ax,var in zip(axes_list,variables_to_plot):
        get_timeseries_plot(ax, times, param_lists[1][::], variable_list[var], A_v_ct_v_d_data[3,:,1,var],var,"$U$",legend_flag=True)
    pp.tight_layout()
    pp.savefig(dir + plot_folder + "timeseries_A_U_D_good_marginal" + imgformat)

    variables_to_plot = [0,1, 2, 3, 4, 5]

    fig, axes_list = pp.subplots(len(variables_to_plot), 1, figsize=(18,4 * len(variables_to_plot)))
    print(str(param_lists[0][6]) + ", " + str(param_lists[2][3]))
    for ax,var in zip(axes_list,variables_to_plot):
        get_timeseries_plot(ax, times, param_lists[1][::], variable_list[var], A_v_ct_v_d_data[6,:,3,var],var,"$U$",legend_flag=True)
    pp.tight_layout()
    pp.savefig(dir + plot_folder + "timeseries_A_U_D_bad_marginal" + imgformat)

    plot_marginal_gain([5,10,20],param_lists[0],param_lists[2],param_lists[1],A_v_ct_v_d_data,
                       "Marginal returns on social incentives with respect to $d, A$","$A$ (intra-patch infestation rate)","$d$ (inter-patch infestation rate)",
                       dir + plot_folder + "A_v_ct_v_d_marginal_gain",dt,times)

    point_3d = 0.11
    plot_3d_inset(times,param_lists[0],list(range(5,40,2)),param_lists[1],np.transpose(A_v_ct_v_d_data[:,:,6,:,:],(1,0,2,3)),
                  "Marginal returns on social incentives ($U$) with respect to $A$ and time",
                  "$A$ (intra-patch infestation rate)",
                  dir + plot_folder + "A_v_time" + imgformat,
                  dir + plot_folder + "A_v_time_3d_inset" + imgformat,
                  dt,
                  point_3d)

def graph_control_plots():
    times,param_lists,node_control_betweenness_data = get_data_from_folder(
        "simulation_output/control_delay_dt_graph_adj_quarantine.npy", num_timeseries)
    print(param_lists)
    for i,t_0 in enumerate(param_lists[0]):
        parameter_plane([5,10,20],param_lists[1], param_lists[2],node_control_betweenness_data[i,:,:,4,:],
                        "$\Delta t$, quarantine duration",
                        "$|V|$, size of quarantined set",
                        "Total infested, T(t), with respect to quarantine duration ($\Delta t$), and size of quarantine ($|V|$), $t_0$ = " + str(t_0),
                        dir + plot_folder + "node_quarantine_plot_" + str(t_0)+imgformat,
                        0.01)

f_v_ct_v_ce_plots()
A_v_ct_v_d_plots()
#graph_control_plots()