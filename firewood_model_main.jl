using OrdinaryDiffEq
using LightGraphs,SimpleWeightedGraphs
using Random
using Parameters
using DataStructures
using PyCall
using SparseArrays
using MuladdMacro
using LinearAlgebra
using StatsBase
LinearAlgebra.BLAS.set_num_threads(8)
@with_kw mutable struct parameters_struct
    dims::Int64
    epsilon::Float64 = 1.4
    A::Float64 = 9e-4
    C_e::Float64 = 0.0
    I_a::Float64 = 1.0
    U::Float64 = 0.0
    f::Float64 = 0.1
    d::Float64 = 0.1
    K::Float64 = 5000
    dt::Float64 = 0.0
    r::Float64 = 0.02
    control_delay::Float64 = 0.0
    s::Float64 = 0.1
    sigma::Float64 = 0.1
    
    graph_adj::SparseMatrixCSC
    out_degree_vector::Array{Float64,1} = graph_adj*ones(dims)
    graph_adj_quarantine::SparseMatrixCSC
    out_degree_vector_quarantine::Array{Float64,1} = graph_adj_quarantine*ones(dims)
end
function array_sigmoid(x::Array{T,1})::Array{T,1} where {T<:Real}
    return 1 ./ (1 .+ exp.(-1*x))
end

@views @muladd function diff_model_quarantine_adj_vectorized(du,u,p,t)
    params,cache,cache2 = p
    S = u[1:params.dims]
    I = u[params.dims+1:params.dims*2]
    B = u[params.dims*2+1:params.dims*3]
    L = u[params.dims*3+1:params.dims*4]
    T = u[params.dims*4+1:params.dims*5]
    cache2 = array_sigmoid(I .- params.I_a)
    if t>=params.control_delay && t<params.control_delay+params.dt
        mul!(cache,params.graph_adj_quarantine,(I .* (1 .- L)))
        du[1:params.dims].=params.r .* S .* (1 .- (S .+ I) ./ params.K) .- params.A .* S .* (I .+ B).*cache2#S+1
        du[params.dims+1:params.dims*2].= -1 * params.epsilon .* I + params.A .* S .* (I .+ B).*cache2 - (params.d) .* (1 - params.C_e) .* params.out_degree_vector_quarantine .*  I .* (1 .- L)
        du[params.dims*2+1:params.dims*3].= -1 * params.epsilon .* B + (params.d) * (1 - params.C_e) .*  cache#B_i+1
        du[params.dims*3+1:params.dims*4].= params.sigma .* L .* (1 .- L) .* (params.U .+ params.s .* (2 .* L .- 1) + params.f .*I) #L_i+1
        du[params.dims*4+1:params.dims*5].= params.A .* S .* (I .+ B).*cache2 #Total infested trees
    else
        mul!(cache,params.graph_adj,(I .* (1 .- L)))
        du[1:params.dims].=params.r .* S .* (1 .- (S .+ I) ./ params.K) .- params.A .* S .* (I .+ B).*cache2#S+1
        du[params.dims+1:params.dims*2].= -1 * params.epsilon .* I + params.A .* S .* (I .+ B).*cache2 - (params.d) .* (1 - params.C_e) .* params.out_degree_vector .* I .* (1 .- L)
        du[params.dims*2+1:params.dims*3].= -1 * params.epsilon .* B + (params.d) .* (1 - params.C_e).*cache 
        du[params.dims*3+1:params.dims*4].= params.sigma .* L .* (1 .- L) .* (params.U .+ params.s .* (2 .* L .- 1) + params.f .*I) #L_i+1
        du[params.dims*4+1:params.dims*5].= params.A .* S .* (I .+ B).*cache2 #Total infested trees
    end
end

function import_graph_data(filename::String = "julia_format_large_core.npy")
    np = pyimport("numpy")
    py_graph_data = np.load("julia_format_large_core.npy",allow_pickle = true)
    sources,destinations,weights,initial_nodes,betweenness_weights = map((x)->convert(Array,x),py_graph_data)
    g = SimpleWeightedDiGraph(sources.+1,destinations.+1,weights)
    normalize_graph_weights!(g)
    return g,[i for (i,b) in enumerate(initial_nodes) if b == true],betweenness_weights
end
function normalize_graph_weights!(g::SimpleWeightedDiGraph)
    for u in vertices(g)
        weight_sum = 0
        for v in neighbors(g,u)
            weight_sum+=g.weights[v,u]
        end
        for v in neighbors(g,u)
            g.weights[v,u] = g.weights[v,u]/weight_sum
        end
    end
end
function get_initial_data(infect_list::Array{Int64,1},dims::Int64)
    
    init = zeros(5,dims)
    susceptible_node = [5000,0,0,0.5,0]
    infest_node = [0,5000,0,0.5,5000]
    for node in 1:dims
        if node in infect_list
            init[:,node].=infest_node
        else
            init[:,node].=susceptible_node
        end
    end
    return vcat(transpose(init)...)
end
function ODE_parameter_plane(infest_list::Array{Int64,1},
    parameter_struct::parameters_struct,
    ranges::Array{Array{Float64,1},1},
    params::Array{Symbol,1},time::Float64,
    quarantine_list::Array{Int64,1})

    initial_conditions = get_initial_data(infest_list,parameter_struct.dims)

    times = []
    println("doin stufff...")
    dims = [1:length(x) for x in ranges]
    
    data_matrix = zeros([length(x) for x in ranges]...,6,trunc(Int64,time/0.01)+1)
    
    #println(size(data_matrix))
    iterator_array = collect(zip(Iterators.product(ranges...),Iterators.product(dims...)))
    for i ∈ 1:length(iterator_array)
        vars,pos = iterator_array[i]
        for (j,field) in enumerate(params)
            if field == :graph_adj_quarantine
                setfield!(parameter_struct,:graph_adj_quarantine,get_quarantine_matrix(vars[j],quarantine_list,parameter_struct.graph_adj))
                setfield!(parameter_struct,:out_degree_vector_quarantine, parameter_struct.graph_adj_quarantine*ones(parameter_struct.dims))
            else
                setfield!(parameter_struct, field, vars[j])
            end
        end
        problem = ODEProblem(diff_model_quarantine_adj_vectorized,initial_conditions,(0,time),(parameter_struct,zeros(parameter_struct.dims),zeros(parameter_struct.dims)),saveat = 0.01)
        sol = solve(problem,Tsit5(),maxiters = 1e7,reltol = 1e-9,abstol = 1e-10)
        times = sol.t
        output_matrix = zeros(6,length(sol.t))
        for (j,l) in enumerate(sol.u)
                component_list = [l[1+parameter_struct.dims*k:parameter_struct.dims*(k+1)] for k ∈ 0:4]
                
                output_matrix[1:5,j].= map(mean,component_list)
                output_matrix[6,j] = count(component_list[2] .>= 1.0)
        end
        data_matrix[pos...,:,:] .= output_matrix
    end
    np = pyimport("numpy")
    np.save("simulation_output/"*join(params,"_")*".npy",PyObject((times,data_matrix,ranges)))

    println("done ", "simulation_output/"*join(params,"_")*".npy")
end
function get_quarantine_matrix(size::Float64, quarantine_list::Array{Int64,1}, adj_matrix::SparseMatrixCSC)::SparseMatrixCSC
        quarantine_matrix = deepcopy(adj_matrix)
        for v in quarantine_list[1:trunc(Int64,size)]
            quarantine_matrix[v+1,:].=0
            quarantine_matrix[:,v+1].=0
        end
        dropzeros!(quarantine_matrix)
        return quarantine_matrix
end
  

function timeseries(time::Float64, parameters::parameters_struct,infest_list::Array{Int64,1})
    initial_conditions = get_initial_data(infest_list,parameters.dims)
    problem = ODEProblem(diff_model_quarantine_adj_vectorized,initial_conditions,(0,time),(parameters,zeros(parameters.dims),zeros(parameters.dims)),saveat = 0.01)
    sol = solve(problem,Tsit5())
    output_matrix = zeros(length(sol.t),5)
    for (i,l) in enumerate(sol.u)
            component_list = [l[1+parameters.dims*k:parameters.dims*(k+1)] for k ∈ 0:4]
            output_matrix[i,:].= map(mean,component_list)
     end
    return sol.t,output_matrix
end


g,infest_list,quarantine_list = import_graph_data()
problem_size = nv(g)
println(problem_size)
adj_matrix = adjacency_matrix(g)

A_U_D_vars = map((x) -> convert(Array,x),[LinRange(0.0006,0.0016,10),LinRange(-5,5,6),LinRange(0.005,0.3,10)])
F_U_C_e_vars = map((x) -> convert(Array,x),[LinRange(0.01,0.13,10),collect(-5.0:5.0),LinRange(0.0,1.0,10)])
F_U_C_e_fields = [:f,:U, :C_e]
quarantine_vars = [collect(1.0:0.5:5.0),collect(1.0:0.5:5.0),collect(0.0:50:500)]
quarantine_fields = [:control_delay,:dt,:graph_adj_quarantine]
A_U_D_fields = [:A,:U,:d]


parameters_quarantine = parameters_struct(dims = problem_size,graph_adj = adj_matrix,graph_adj_quarantine = adj_matrix)
task1 = Threads.@spawn ODE_parameter_plane(infest_list,parameters_quarantine,quarantine_vars,quarantine_fields,21.0,quarantine_list)

parameters_A_U_D = parameters_struct(dims = problem_size,graph_adj = adj_matrix,graph_adj_quarantine = adj_matrix)
task2 = Threads.@spawn ODE_parameter_plane(infest_list,parameters_A_U_D,A_U_D_vars,A_U_D_fields,40.0,Int64[])

parameters_F_U_C = parameters_struct(dims = problem_size,graph_adj = adj_matrix,graph_adj_quarantine = adj_matrix)
task3 = Threads.@spawn ODE_parameter_plane(infest_list,parameters_F_U_C,F_U_C_e_vars,F_U_C_e_fields,40.0,Int64[])

map(fetch,[task1,task2,task3])