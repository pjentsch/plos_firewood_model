# Forest-pest-firewood-model
"Go big or go home: a model-based assessment of general strategies to slow the spread of forest pests via infested firewood"

Peter C. Jentsch, Chris T. Bauch, Denys Yemshanov, Madhur Anand

The model code is written Julia, and located in "firewood_model_main.jl".


 "julia_format_large_core.npy" contains the weighted digraph used in the study, in numpy format, and can be opened by the load function in that python library.

The file "data_parser.py" calculates the 10-core of the graph contained in the data, plus the list of betweenness-centralities, and packs these with the Toronto node IDs into "julia_format_large_core.npy".
Unfortunately, we are unable to make the underlying source data available to the public. Please contact the authors if you are interested in looking at this.

The model script calculates three parameter planes and puts their output into the "simulation_output" directory, which is then parsed by "plot.py" to generate the figures in the manuscript, except the first figure, which was also created with ArcGIS.

Steps to reproduce figures:

0. Clone the repository


1. Ensure Julia v1.4 or greater is installed, and the following packages:

- OrdinaryDiffEq
- LightGraphs,SimpleWeightedGraphs
- Random
- Parameters
- DataStructures
- PyCall
- SparseArrays
- MuladdMacro
- LinearAlgebra
- StatsBase

additionally, you will need a recent version of python, with matplotlib, numpy, seaborn and networkx.

2. Run firewood_model_main.jl with julia, you should see three files added to the simulation_output folder.

3. Run plot.py with python, which will add all of the plot files to /plots/. The exception is the network map, which was created in ArcGIS manually.



Thank you!
Peter C Jentsch
