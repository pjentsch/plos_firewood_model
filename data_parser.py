import networkx as nx
import re
import collections
import numpy as np


dir = "data\\"

def save_graph_as_arcgis(graph):
    pts_directory = {}

    outfile = open("core_routes.txt", 'w')

    pts = open(dir + "EAB_points.csv")


    for line in pts.readlines()[1:]:
        line_format = re.split(',|\n|', line)
        pts_directory[line_format[2]] = (line_format[8], line_format[9])

    for i, edge in enumerate(graph.edges()):
        dat = graph.get_edge_data(edge[0], edge[1])
        start = edge[0]
        end = edge[1]
        trips = str(dat['weight'])
        if start in pts_directory and end in pts_directory:
            writestring = str(i) + ";" + trips + ";LINESTRING(" + pts_directory[start][0] + " " + pts_directory[start][
                1] + "," + pts_directory[end][0] + " " + pts_directory[end][1] + ")"
            outfile.write(writestring + "\n")

    outfile.close()

def import_routes_as_digraph(pts_file,network_dat,init_pts_file):
    # import infection init points
    initlist = []
    for line in init_pts_file.readlines()[1:-1]:
        line_format = re.split('[,\n \t]', line)[:-1]
        initlist.append(line_format[2])
    infest_data.close()
    points = pts_file.read().split("\n")

    data = network_dat.read().split('\n')[1].split('|')
    G = nx.DiGraph()
    for current_node, line in enumerate(data):
        G.add_node(points[current_node])
        for end_node, entry in enumerate(re.split('[,\n \t]', line)[:-1]):

            entry = float(entry)
            if entry != 0:
                G.add_edge(points[current_node], points[end_node], weight=entry)

    nx.set_node_attributes(G, False, "initial_infestation")
    for v in G.nodes():
        if v in initlist:
            G.nodes[v]["initial_infestation"] = True
    network_dat.close()
    return (G, initlist)

network_dat = open(dir + "EAB_trips_array.txt")
pts_file = open(dir + "localpointlist.txt")
infest_data = open(dir + "toronto_points.csv", 'r')

def networkx_to_julia(graph,filename): #sort of
    graph_numbered = nx.convert_node_labels_to_integers(graph)
    edges = graph_numbered.edges()
    print("begin")
    sources, destinations = list(zip(*edges))
    weights = [w for (u,v,w) in graph_numbered.edges.data('weight')]
    node_list = [a for (v,a) in graph_numbered.nodes(data = "initial_infestation")]

    max_weight = sorted(weights,reverse=True)[0]
    print(max_weight)
    for u,v,d in graph_numbered.edges(data = True):
        d['weight'] = max_weight + 1 - d['weight']
    betweenness_nodes = nx.betweenness_centrality(graph_numbered,weight='weight')
    betweenness_list = list(betweenness_nodes.items())
    sorted_betweenness_list = sorted(betweenness_list, key = lambda x: x[1],reverse = True)
    sorted_betweenness_nodes = [x[0] for x in sorted_betweenness_list]
    print(sorted_betweenness_nodes)
    np.save(filename,[sources,destinations,weights,node_list,sorted_betweenness_nodes])

data_graph,infest_list = import_routes_as_digraph(pts_file,network_dat,infest_data)
large_core = nx.algorithms.k_core(data_graph,10)
networkx_to_julia(large_core,"julia_format_large_core")